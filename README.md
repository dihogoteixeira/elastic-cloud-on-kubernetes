## Install the ECK Operator

Precisamos provisionar um Operador de ECK para facilitar nossa gerencia de configurações e recursos no cluster de ECK, 
contudo utilizamos o Helm para provisionar mas outras maneiras podem ser encontradas em *Deploy ECK in your Kubernetes cluster*:

```sh
https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-deploy-eck.html
```

Provisionaremos aqui, o elastic-operator de maneira que ele "escute" os namespaces `default`,
`elastic-system` e `kube-system`. 

De modo que poçamos colocar nossos recursos em um desses namespaces:

```sh
helm install elastic-operator elastic/eck-operator -n elastic-system --create-namespace \
  --set=installCRDs=true \
  --set=managedNamespaces='{default, elastic-system, kube-system}' \
  --set=createClusterScopedResources=true \
  --set=webhook.enabled=true \
  --set=config.validateStorageClass=false 
```

Validando os logs de subida do `elastic-operator`:

```sh
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
```

## Create ELASTIC CLUSTER and KIBANA

0. Clone repo:

```sh
git clone https://gitlab.com/dihogoteixeira/elastic-cloud-on-kubernetes.git
cd elastic-cloud-on-kubernetes
```

1. Apply ECK PV and PVC

```sh
kubectl apply eck-volume/
```

2. Apply ECK Cluster

```sh
kubectl apply eck-cluster/
```

3. Apply ECK Ingress

```sh
kubectl apply eck-ingress/
```

## Request Elasticsearch access

A ClusterIP Service is automatically created for your cluster:

```sh
kubectl get service data-es-es-http
```
```sh
NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
data-es-es-http      ClusterIP   10.15.251.145   <none>        9200/TCP   34m

```

1. Get the credentials.

A default user named elastic is automatically created with the password stored in a Kubernetes secret:

```sh
PASSWORD=$(kubectl get secret data-es-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')
```

2. Request the Elasticsearch endpoint.

From inside the Kubernetes cluster:

```sh
curl -u "elastic:$PASSWORD" -k "https://data-es-es-http:9200"
```

From your local workstation, use the following command in a separate terminal:

```sh
kubectl port-forward service/data-es-es-http 9200
```

Then request localhost:

```sh
curl -u "elastic:$PASSWORD" -k "http://localhost:9200"
```

Example:

```sh
[root@vm1-bootcamp-1a ~]# curl -u "elastic:$PASSWORD" -k "http://localhost:9200/"
{
  "name" : "data-es-es-default-0",
  "cluster_name" : "data-es",
  "cluster_uuid" : "WeoXuE60Qvir0Yelv3t_Cw",
  "version" : {
    ...
  },
  "tagline" : "You Know, for Search"
}
[root@vm1-bootcamp-1a ~]#
```